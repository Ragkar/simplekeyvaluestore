use criterion::{criterion_group, criterion_main, Criterion};
use rand::Rng;
use tempfile::TempDir;

use kvs::{KvStore, SledKvsEngine, KvsEngine};

fn get_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("get_bench");
    group.bench_function("kvs", |b| {
        let tmp_dir = TempDir::new().unwrap();
        let mut store = KvStore::open(tmp_dir.into_path()).unwrap();
        let mut rng = rand::thread_rng();

        let i = 10000;
        for i in 0..i {
            store.set(format!("{}", i), "value".to_string()).unwrap();
        }

        b.iter(|| {
            store.get(format!("{}", rng.gen_range(0, i - 1))).unwrap();
        })
    });
    group.bench_function("sled", |b| {
        let tmp_dir = TempDir::new().unwrap();
        let mut store = KvStore::open(tmp_dir.into_path()).unwrap();
        let mut rng = rand::thread_rng();

        let i = 10000;
        for i in 0..i {
            store.set(format!("{}", i), "value".to_string()).unwrap();
        }

        b.iter(|| {
            store.get(format!("{}", rng.gen_range(0, i - 1))).unwrap();
        })
    });
}

fn set_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("set_bench");
    group.bench_function("kvs", |b| {
        let tmp_dir = TempDir::new().unwrap();
        let mut store = KvStore::open(tmp_dir.into_path()).unwrap();
        let mut rng = rand::thread_rng();
        b.iter(|| {
            store.set(format!("{}", rng.gen_range(0, 10000)), "v".to_string()).unwrap();
        })
    });
    group.bench_function("sled", |b| {
        let tmp_dir = TempDir::new().unwrap();
        let mut store = SledKvsEngine::open(tmp_dir.into_path()).unwrap();
        let mut rng = rand::thread_rng();
        b.iter(|| {
            store.set(format!("{}", rng.gen_range(0, 10000)), "v".to_string()).unwrap();
        })
    });
}

criterion_group!(benches, get_bench, set_bench);
criterion_main!(benches);
