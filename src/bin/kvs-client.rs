extern crate structopt;
use structopt::StructOpt;

use std::net::TcpStream;
use std::process::exit;

use kvs::{Request, Response, write_message, read_message};

const DEFAULT_ADDRESS: &str = "127.0.0.1:4000";

/// Subcommand of the kvs-client cli
#[derive(Debug, StructOpt)]
enum Subcmd {
    Get {
        key: String,
        #[structopt(long, default_value = DEFAULT_ADDRESS)]
        addr: String,
    },
    Rm {
        key: String,
        #[structopt(long, default_value = DEFAULT_ADDRESS)]
        addr: String,
    },
    Set {
        key: String,
        value: String,
        #[structopt(long, default_value = DEFAULT_ADDRESS)]
        addr: String,
    },
}

#[derive(Debug, StructOpt)]
#[structopt(
    about = env!("CARGO_PKG_DESCRIPTION"),
    name = env!("CARGO_PKG_NAME"),
    version = env!("CARGO_PKG_VERSION"),
)]
struct Cli {
    #[structopt(subcommand)]
    subcmd: Subcmd,
}

fn main() {
    let cli = Cli::from_args();

    // Build request
    let (request, addr, cmd_is_get) = match cli.subcmd {
        Subcmd::Get { key, addr } => (Request::Get { key }, addr, true),
        Subcmd::Rm { key, addr } => (Request::Remove { key }, addr, false),
        Subcmd::Set { key, value, addr } => (Request::Set { key, value }, addr, false),
    };
    let mut stream = TcpStream::connect(addr).unwrap();

    // Send request
    write_message::<Request>(&request, &mut stream).unwrap();

    // Get response
    let response = read_message::<Response>(&mut stream).unwrap();
    match response {
        Response::Ok(Some(v)) => {
            if cmd_is_get {
                println!("{}", v);
            }
        }
        Response::Ok(None) => {
            if cmd_is_get {
                println!("Key not found");
            }
        }
        Response::Err(e) => {
            eprintln!("{}", e);
            exit(1);
        }
    }
}
