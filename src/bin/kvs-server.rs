use structopt::StructOpt;

extern crate log;
use log::{error, info};

use std::net::{TcpListener, TcpStream};
use std::sync::Arc;
use std::sync::atomic::{AtomicU64, Ordering};

use kvs::{
    KvStore,
    KvsEngine,
    KvsError,
    Request,
    Response,
    Result,
    SledKvsEngine,
    read_message,
    write_message,
};

/// Argument of the kvs-server cli
#[derive(Debug, StructOpt)]
#[structopt(
    about = env!("CARGO_PKG_DESCRIPTION"),
    name = env!("CARGO_PKG_NAME"),
    version = env!("CARGO_PKG_VERSION"),
)]
struct Cli {
    #[structopt(long, default_value = "127.0.0.1:4000")]
    addr: String,
    #[structopt(long, default_value = "kvs")]
    engine: String,
}

fn main() {
    stderrlog::new()
        .module(module_path!())
        .verbosity(4)
        .init()
        .unwrap();

    let cli = Cli::from_args();

    info!("Version = {}", env!("CARGO_PKG_VERSION"));
    info!("Engine = {}", cli.engine);
    info!("Address = {}", cli.addr);

    if cli.engine == "kvs" {
        KvsServer::<KvStore>::new(&cli.addr, &cli.engine).unwrap()
            .run().unwrap();
    } else if cli.engine == "sled" {
        KvsServer::<SledKvsEngine>::new(&cli.addr, &cli.engine).unwrap()
            .run().unwrap();
    } else {
        error!("{}", KvsError::InvalidEngineName);
    }
}

struct KvsServer<Engine: KvsEngine> {
    addr: String,
    engine: Engine,
}

impl<Engine: KvsEngine> KvsServer<Engine> {
    pub fn new(addr: &str, engine_name:&str) -> Result<Self> {
        let engine = KvsServer::build_from(engine_name)?;
        Ok(KvsServer { addr: addr.to_string(), engine })
    }

    fn build_from(engine_name: &str) -> Result<Engine> {
        let dir = std::env::current_dir().unwrap();
        let engine_file = dir.join("engine.txt");

        match std::fs::read_to_string(&engine_file) {
            Ok(s) => if s != engine_name {
                return Err(KvsError::InvalidEngineDirectory);
            }
            Err(_) => std::fs::write(engine_file, &engine_name)?,
        };
        Engine::open(dir)
    }

    pub fn run(&mut self) -> Result<()> {
        let thread_counter = Arc::new(AtomicU64::new(0));
        let listener = TcpListener::bind(&self.addr)?;
        for connection in listener.incoming() {
            while thread_counter.load(Ordering::Relaxed) >= 4 {
                std::thread::sleep(std::time::Duration::from_millis(100));
            }

            let thread_counter_clone = thread_counter.clone();
            let engine = self.engine.clone();
            std::thread::spawn(move || {
                thread_counter_clone.fetch_add(1, Ordering::Relaxed);
                match connection {
                    Ok(mut stream) => {
                        let response = handle_connection(&mut stream, engine);
                        write_message::<Response>(&response, &mut stream).unwrap();
                    }
                    Err(e) => eprintln!("{}", e),
                };
                thread_counter_clone.fetch_sub(1, Ordering::Relaxed);
            });
        }
        Ok(())
    }
}


fn handle_connection<Engine: KvsEngine>(stream: &mut TcpStream, engine: Engine) -> Response {
    match read_message::<Request>(stream) {
        Ok(request) => handle_request(request, engine),
        Err(e) => Response::Err(format!("Fail to read buffer from stream ({})", e)),
    }
}

fn handle_request<Engine: KvsEngine>(request: Request, engine: Engine) -> Response {
    match request {
        Request::Get { key } => match engine.get(key) {
            Ok(v) => Response::Ok(v),
            Err(e) => Response::Err(format!("{}", e)),
        },
        Request::Remove { key } => match engine.remove(key) {
            Ok(()) => Response::Ok(None),
            Err(e) => Response::Err(format!("{}", e)),
        },
        Request::Set { key, value } => match engine.set(key, value) {
            Ok(()) => Response::Ok(None),
            Err(e) => Response::Err(format!("{}", e)),
        },
    }
}