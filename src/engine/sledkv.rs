use std::ops::Deref;
use std::path::PathBuf;

use crate::{KvsEngine, KvsError, Result};

/// `KvsEngine` using sled
#[derive(Clone)]
pub struct SledKvsEngine {
    db: sled::Db,
}

impl KvsEngine for SledKvsEngine {
    fn get(&self, key: String) -> Result<Option<String>> {
        self.db.get(&key)
            .map_err(Into::<KvsError>::into)?
            .map(|veci| String::from_utf8(veci.deref().to_vec()))
            .map(|res| res.map_err(Into::<KvsError>::into))
            .transpose()
    }

    fn remove(&self, key: String) -> Result<()> {
        self.db.remove(key)
            .map_err(Into::<KvsError>::into)?
            .map_or(Err(KvsError::KeyNotFound), |_| Ok(()))
    }

    fn set(&self, key: String, value: String) -> Result<()> {
        let k = sled::IVec::from(key.as_bytes());
        let v = sled::IVec::from(value.as_bytes());
        self.db.insert(k, v)
            .map_err(Into::<KvsError>::into)
            .map(|_| ())
    }

    fn open(path: impl Into<PathBuf>) -> Result<SledKvsEngine> {
        let db = sled::open(path.into())?;
        Ok(SledKvsEngine { db })
    }
}