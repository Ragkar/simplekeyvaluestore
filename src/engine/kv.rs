use serde::{Deserialize, Serialize};

use crate::pos_buffer::{FReader, FWriter};
use crate::{KvsEngine, KvsError, Result};

use std::cell::{RefCell, RefMut};
use std::collections::{BTreeMap, HashMap};
use std::io::SeekFrom;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex, RwLock};

const MAX_COMPACTION: u64 = 1024 * 1024;

/// Command read/write from log.
#[derive(Debug, Deserialize, Serialize)]
enum Command {
    Set { key: String, value: String },
    Remove { key: String },
}

impl Command {
    fn new_set(key: &str, value: &str) -> Command {
        Command::Set {
            key: key.to_string(),
            value: value.to_string(),
        }
    }

    fn new_remove(key: &str) -> Command {
        Command::Remove {
            key: key.to_string(),
        }
    }
}

/// Information about a `Command` in logs.
#[derive(Clone, Debug)]
struct Log {
    log_file: u64,
    idx: u64,
    len: u64,
}

#[derive(Clone)]
pub struct KvStore {
    index: Arc<RwLock<BTreeMap<String, Log>>>,
    reader: KvsReader,
    writer: Arc<Mutex<KvsWriter>>,
}

impl KvsEngine for KvStore {
    fn get(&self, key: String) -> Result<Option<String>> {
        self.index.read()
            .map_err(|_| KvsError::AsyncError)?
            .get(&key)
            .map(|log| self.reader.get(log))
            .transpose()
            .map(|opt| opt.flatten())
    }

    fn set(&self, key: String, value: String) -> Result<()> {
        self.writer.lock()
            .map_err(|_| KvsError::AsyncError)?
            .set(key, value)
    }

    fn remove(&self, key: String) -> Result<()> {
        self.writer.lock()
            .map_err(|_| KvsError::AsyncError)?
            .remove(key)
    }

    fn open(path: impl Into<PathBuf>) -> Result<Self> {
        let path = path.into();

        let mut index = BTreeMap::new();
        let mut readers = HashMap::new();
        let mut uncompact = 0;

        // Get list of existing log files
        let mut log_files = KvStore::get_log_files(path.as_path())?;
        if log_files.is_empty() {
            log_files.push(1);
        }
        let current_log_file = *log_files.last().unwrap();
        let compact_file = Arc::new(AtomicU64::new(*log_files.first().unwrap()));

        let mut file_path = KvStore::get_path(&path, current_log_file);
        let writer = FWriter::open(&file_path)?;

        // Load log files and create reader on them
        for log_file in log_files {
            file_path = KvStore::get_path(&path, log_file);
            let mut reader = FReader::open(&file_path)?;
            uncompact += KvStore::load(log_file, &mut index, &mut reader)?;
            readers.insert(log_file, reader);
        }

        let arc_index = Arc::new(RwLock::new(index));
        let kvsreader = KvsReader {
            compact_file,
            path: path.clone(),
            readers: RefCell::new(readers)
        };
        let kvswriter = Arc::new(Mutex::new(KvsWriter {
            current_log_file,
            index: arc_index.clone(),
            reader: kvsreader.clone(),
            uncompact,
            writer,
        }));
        Ok(KvStore {
            index: arc_index,
            reader: kvsreader,
            writer: kvswriter,
        })
    }
}

impl KvStore {
    fn load(log_file: u64, map: &mut BTreeMap<String, Log>, reader: &mut FReader) -> Result<u64> {
        let mut uncompact = 0;
        let mut prev_idx = reader.seek(SeekFrom::Start(0))?;
        let mut stream = serde_json::Deserializer::from_reader(reader).into_iter::<Command>();

        while let Some(result_cmd) = stream.next() {
            let cmd = result_cmd?;
            let idx = stream.byte_offset() as u64;
            uncompact += KvStore::load_cmd(log_file, map, idx, prev_idx, cmd);
            prev_idx = idx;
        }
        Ok(uncompact)
    }

    /// Load `Command` to map of Key to `Log`
    ///
    /// Return the number of uncompact bytes,
    fn load_cmd(
        log_file: u64,
        map: &mut BTreeMap<String, Log>,
        idx: u64,
        prev_idx: u64,
        cmd: Command,
    ) -> u64 {
        let mut uncompact = 0;
        match cmd {
            Command::Set { key, value: _ } => {
                let log = Log {
                    log_file,
                    idx: prev_idx,
                    len: idx - prev_idx,
                };
                if let Some(old_log) = map.insert(key, log) {
                    uncompact += old_log.len;
                }
            }
            Command::Remove { key } => {
                if let Some(old_log) = map.remove(&key) {
                    uncompact += old_log.len;
                }
                uncompact += idx - prev_idx;
            }
        };
        uncompact
    }

    fn get_log_files(path: &Path) -> Result<Vec<u64>> {
        let mut file_nums = std::fs::read_dir(&path)?
            .filter_map(|entry| entry.ok())
            .map(|entry| entry.path())
            .filter(|path| path.is_file() && path.extension() == Some("kvslog".as_ref()))
            .flat_map(|path| {
                path.file_name()
                    .and_then(std::ffi::OsStr::to_str)
                    .and_then(|s| s.trim_end_matches(".kvslog").parse::<u64>().ok())
            })
            .collect::<Vec<u64>>();
        file_nums.sort_unstable();
        Ok(file_nums)
    }

    fn get_path(path: &Path, log_file: u64) -> PathBuf {
        path.join(format!("{}.kvslog", log_file))
    }
}


pub struct KvsReader {
    path: PathBuf,
    compact_file: Arc<AtomicU64>,
    readers: RefCell<HashMap<u64, FReader>>,
}

impl Clone for KvsReader {
    fn clone(&self) -> Self {
        KvsReader {
            path: self.path.clone(),
            compact_file: self.compact_file.clone(),
            readers: RefCell::new(HashMap::new())
        }
    }
}

impl KvsReader {
    fn get(&self, log: &Log) -> Result<Option<String>> {
        self.close_old_files();

        self.check_reader(log.log_file)?;
        let mut readers = self.readers.borrow_mut();
        readers.get_mut(&log.log_file)
            .ok_or(KvsError::InvalidReader)
            .and_then(|reader| match KvsReader::get_cmd(log, reader) {
            Ok(Command::Set { key: _, value }) => Ok(Some(value)),
            Ok(_) => Err(KvsError::UnexpectedCommand),
            Err(e) => Err(e)
        })
    }

    fn check_reader(&self, log_file: u64) -> Result<()> {
        let mut readers = self.readers.borrow_mut();
        if !readers.contains_key(&log_file) {
            KvsReader::open_log_file(&self.path, log_file, &mut readers)?
        }
        Ok(())
    }

    fn get_cmd(log: &Log, reader: &mut FReader) -> Result<Command> {
        reader.seek(SeekFrom::Start(log.idx))?;
        let cmd_reader = reader.take(log.len);
        let cmd: Command = serde_json::from_reader(cmd_reader)?;
        Ok(cmd)
    }

    fn open_log_file(path: &Path, log_file: u64, readers: &mut RefMut<HashMap<u64, FReader>>) -> Result<()> {
        let file_path = KvStore::get_path(path, log_file);
        let reader = FReader::open(&file_path)?;
        readers.insert(log_file, reader);
        Ok(())
    }

    fn close_old_files(&self) {
        // Remove unused reader and try to erase old log file
        let compact_file = self.compact_file.load(Ordering::Relaxed);
        let old_log_file: Vec<u64> = self.readers
            .borrow()
            .keys()
            .filter(|&&n| n < compact_file)
            .cloned()
            .collect();
        for k in old_log_file {
            self.readers.borrow_mut().remove(&k);
            if let Err(e) = std::fs::remove_file(KvStore::get_path(&self.path, k)) {
                eprintln!("{}", e);
            }
        }
    }

}

pub struct KvsWriter {
    current_log_file: u64,
    index: Arc<RwLock<BTreeMap<String, Log>>>,
    reader: KvsReader,
    uncompact: u64,
    writer: FWriter
}

impl KvsWriter {
    fn set(&mut self, key: String, value: String) -> Result<()> {
        let cmd = Command::new_set(&key, &value);
        let log = KvsWriter::set_cmd(self.current_log_file, cmd, &mut self.writer)?;
        if let Some(old_log) = self.index.write()
                .map_err(|_| KvsError::AsyncError)?
                .insert(key.to_string(), log) {
            self.uncompact += old_log.len
        }

        if self.uncompact > MAX_COMPACTION {
            self.compact()?;
        }
        Ok(())
    }

    fn set_cmd(log_file: u64, cmd: Command, writer: &mut FWriter) -> Result<Log> {
        let idx = writer.idx;
        serde_json::to_writer(writer.by_ref(), &cmd)?;
        writer.flush()?;
        let len = writer.idx - idx;

        Ok(Log { log_file, idx, len })
    }

    fn remove(&mut self, key: String) -> Result<()> {
        // TODO: Should it write command even if key does not exist ?
        if let Some(old_log) = self.index.write()
                .map_err(|_| KvsError::AsyncError)?
                .remove(&key) {
            let cmd = Command::new_remove(&key);
            let prev_idx = self.writer.idx;

            serde_json::to_writer(&mut self.writer, &cmd)?;
            self.writer.flush()?;

            self.uncompact += old_log.len + (self.writer.idx - prev_idx);
            Ok(())
        } else {
            Err(KvsError::KeyNotFound)
        }
    }

    fn compact(&mut self) -> Result<()> {
        // Create compaction file
        let compact_file = self.current_log_file + 1;
        let compact_path = KvStore::get_path(&self.reader.path, compact_file);
        let mut writer = FWriter::open(&compact_path)?;
        let reader = FReader::open(&compact_path)?;
        self.reader.readers.borrow_mut().insert(compact_file, reader);

        // Update index map
        let mut new_map = BTreeMap::new();
        for (key, log) in self.index.read().map_err(|_| KvsError::AsyncError)?.iter() {
            // TODO: it can be faster by avoiding deserialization and serialization
            self.reader.check_reader(log.log_file)?;
            let new_log = self.reader.readers
                .borrow_mut()
                .get_mut(&log.log_file)
                .ok_or(KvsError::InvalidReader)
                .and_then(|reader| KvsReader::get_cmd(log, reader))
                .and_then(|cmd| KvsWriter::set_cmd(compact_file, cmd, &mut writer))?;
            new_map.insert(key.clone(), new_log);
        }

        self.update(compact_file, new_map)
    }

    fn update(&mut self, compact_file: u64, map: BTreeMap<String, Log>) -> Result<()> {
        // TODO: Should the new current file be the compact one ?

        let new_file = compact_file + 1;
        let new_path = KvStore::get_path(&self.reader.path, new_file);
        let new_writer = FWriter::open(&new_path)?;
        let new_reader = FReader::open(&new_path)?;
        self.writer = new_writer;
        self.reader.readers.borrow_mut().insert(new_file, new_reader);
        self.current_log_file = new_file;
        self.uncompact = 0;

        self.reader.compact_file.store(compact_file, Ordering::Relaxed);
        *self.index.write().map_err(|_| KvsError::AsyncError)? = map;
        self.reader.close_old_files();

        Ok(())
    }
}