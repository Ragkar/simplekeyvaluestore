use crate::Result;
use std::path::PathBuf;

pub trait KvsEngine: Clone + Send + 'static {
    fn open(path: impl Into<PathBuf>) -> Result<Self>;
    fn get(&self, key: String) -> Result<Option<String>>;
    fn set(&self, key: String, value: String) -> Result<()>;
    fn remove(&self, key: String) -> Result<()>;
}