use failure::Fail;

use std::io;

/// Error types for KvStore
#[derive(Debug, Fail)]
pub enum KvsError {
    /// Async error
    #[fail(display = "Async error")]
    AsyncError,

    /// I/O error
    #[fail(display = "{}", _0)]
    Io(#[cause] io::Error),

    /// Key not found in store
    #[fail(display = "Key not found")]
    KeyNotFound,

    /// Serialization/Deserialisation error
    #[fail(display = "{}", _0)]
    Serde(#[cause] serde_json::Error),

    /// Invalid engine directory
    #[fail(display = "Invalid engine directory")]
    InvalidEngineDirectory,

    /// Invalid engine name
    #[fail(display = "Invalid engine name")]
    InvalidEngineName,

    /// Invalid log file reader
    #[fail(display = "Invalid log file reader")]
    InvalidReader,

    /// Sled error
    #[fail(display = "{}", _0)]
    Sled(#[cause] sled::Error),

    /// Unexpected command in log file
    #[fail(display = "Unexpected command")]
    UnexpectedCommand,

    /// UTF8 error
    #[fail(display = "UTF8 error")]
    Utf8Conversion,
}

impl From<io::Error> for KvsError {
    fn from(err: io::Error) -> KvsError {
        KvsError::Io(err)
    }
}

impl From<serde_json::Error> for KvsError {
    fn from(err: serde_json::Error) -> KvsError {
        KvsError::Serde(err)
    }
}

impl From<sled::Error> for KvsError {
    fn from(err: sled::Error) -> KvsError {
        KvsError::Sled(err)
    }
}

impl From<std::string::FromUtf8Error> for KvsError {
    fn from(_: std::string::FromUtf8Error) -> KvsError {
        KvsError::Utf8Conversion
    }
}

/// Result type for KvStore
pub type Result<T> = std::result::Result<T, KvsError>;
