use crate::Result;

use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufReader, BufWriter, SeekFrom};
use std::path::Path;

/// Writer of log file with position
pub struct FWriter {
    pub writer: BufWriter<File>,
    pub idx: u64,
}

impl FWriter {
    /// Create `FWriter` from `File`
    pub fn new(mut file: File) -> Result<Self> {
        let idx = file.seek(SeekFrom::End(0))?;
        Ok(FWriter {
            writer: BufWriter::new(file),
            idx,
        })
    }

    /// Create `FWriter` from `Path`
    pub fn open(path: &Path) -> Result<Self> {
        let file = OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .open(path)?;
        FWriter::new(file)
    }
}

impl Write for FWriter {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let len = self.writer.write(buf)?;
        self.idx += len as u64;
        Ok(len)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.writer.flush()
    }
}

impl Seek for FWriter {
    fn seek(&mut self, idx: SeekFrom) -> std::io::Result<u64> {
        self.idx = self.writer.seek(idx)?;
        Ok(self.idx)
    }
}

/// Reader of log file with position
pub struct FReader {
    pub reader: BufReader<File>,
    pub idx: u64,
}

impl FReader {
    /// Create `FReader` from `File`
    pub fn new(mut file: File) -> Result<Self> {
        let idx = file.seek(SeekFrom::Current(0))?;
        Ok(FReader {
            reader: BufReader::new(file),
            idx,
        })
    }

    /// Create `FReader` from `Path`
    pub fn open(path: &Path) -> Result<Self> {
        let file = File::open(path)?;
        FReader::new(file)
    }
}

impl Read for FReader {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let len = self.reader.read(buf)?;
        self.idx += len as u64;
        Ok(len)
    }
}

impl Seek for FReader {
    fn seek(&mut self, idx: SeekFrom) -> std::io::Result<u64> {
        self.idx = self.reader.seek(idx)?;
        Ok(self.idx)
    }
}
