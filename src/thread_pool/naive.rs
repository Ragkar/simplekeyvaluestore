use crate::{Result, ThreadPool};

use std::sync::Arc;
use std::sync::atomic::{AtomicU32, Ordering};

pub struct NaiveThreadPool {
    threads: u32,
    counter: Arc<AtomicU32>,
}

impl ThreadPool for NaiveThreadPool {
    fn new(threads: u32) -> Result<Self> {
        Ok(NaiveThreadPool {
            threads,
            counter: Arc::new(AtomicU32::new(threads))
        })
    }

    fn spawn<F>(&mut self, job: F) where F: FnOnce() + Send + 'static {
        while self.counter.load(Ordering::Relaxed) >= self.threads {
            std::thread::sleep(std::time::Duration::from_millis(100));
        }
        let counter = self.counter.clone();

        std::thread::spawn(move || {
            counter.fetch_add(1, Ordering::Relaxed);
            job();
            counter.fetch_sub(1, Ordering::Relaxed);
        });
    }
}