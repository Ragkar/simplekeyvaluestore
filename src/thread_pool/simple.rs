use crate::{Result, ThreadPool};

use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};

type Job = dyn FnOnce() + Send + 'static;

pub struct SimpleThreadPool {
    available: Vec<usize>,
    receiver: Receiver<usize>,
    senders: Vec<Sender<Box<Job>>>,
}

struct ThreadSpawned {
    id: usize,
    receiver: Receiver<Box<Job>>,
    sender: Sender<usize>,
}

// TODO: handle errors
impl ThreadSpawned {
    fn run(&self) {
        let mut run = true;
        while run {
            match self.receiver.recv() {
                Ok(job) => job(),
                Err(_) => run = false,
            }
            if self.sender.send(self.id).is_err() {
                run = false;
            }
        }
    }
}

impl ThreadPool for SimpleThreadPool {
    fn new(threads: u32) -> Result<Self> {
        let (num_tx, num_rx) = channel();
        let mut senders = Vec::new();
        for _ in 0..threads {
            let (job_tx, job_rx) = channel();
            senders.push(job_tx);
            let thread = ThreadSpawned {
                id: senders.len() - 1,
                receiver: job_rx,
                sender: num_tx.clone(),
            };
            std::thread::spawn(move || { thread.run(); });
        }

        Ok(SimpleThreadPool {
            available: Vec::new(),
            receiver: num_rx,
            senders,
        })
    }

    fn spawn<F>(&mut self, job: F) where F: FnOnce() + Send + 'static {
        while self.available.is_empty() {
            let mut run = true;
            while run {
                match self.receiver.try_recv() {
                    Ok(n) => self.available.push(n),
                    Err(TryRecvError::Empty) => run = false,
                    /* TODO */
                    Err(TryRecvError::Disconnected) => run = false,
                }
            }
        }

        if let Some(n) = self.available.pop() {
            let msg = Box::new(job);
            // TODO: handle sender error
            self.senders.get(n).map(|sender| sender.send(msg));
        }
    }
}