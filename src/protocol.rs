use crate::{KvsError, Result};

use serde::{Deserialize, Serialize};
use serde::de::DeserializeOwned;
use std::io::{Read, Write};
use std::net::TcpStream;

/// Request send to kvs-server
#[derive(Deserialize, Serialize)]
pub enum Request {
    Get { key: String },
    Remove { key: String },
    Set { key: String, value: String },
}

pub fn write_message<T: Serialize>(request: &T, stream: &mut TcpStream) -> Result<()> {
    let vec = serde_json::to_vec(request)?;
    let len = vec.len().to_le_bytes();
    stream.write_all(&len)?;
    stream.write_all(&vec)?;
    stream.flush()?;
    Ok(())
}

pub fn read_message<T: DeserializeOwned>(stream: &mut TcpStream) -> Result<T> {
    let mut len_buf = [0; 8];
    stream.read_exact(&mut len_buf)?;
    let len = usize::from_le_bytes(len_buf);

    let mut vec_buf = vec![0; len];
    stream.read_exact(&mut vec_buf)?;
    serde_json::from_slice(&vec_buf)
        .map_err(Into::<KvsError>::into)
}

/// Response send by kvs-server
#[derive(Deserialize, Serialize)]
pub enum Response {
    Ok(Option<String>),
    Err(String),
}